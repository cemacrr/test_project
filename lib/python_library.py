def function_a(arg_a, arg_b):
    """
    This is function a, which adds two values 

    :param arg_a: first argument is a ``float``
    :param arg_b: second argument in an ``int``
    :return: arg_a + arg_b

    Example usage::

      >>> from python_library import function_a
      >>> function_a(2, 3)
      5
    """
    return arg_a + arg_b

def function_b(arg_a=10, arg_b=10):
    """
    This is function b, which multiplies two values

    :param arg_a: first argument
    :param arg_b: second argument
    :return: arg_a * arg_b

    Example usage::

      >>> from python_library import function_b
      >>> function_b(2, 3)
      6
    """
    return arg_a * arg_b

def function_c(arg_a=50, arg_b=25):
    """
    This is function c, which subtracts one value form another

    :param arg_a: first argument
    :param arg_b: second argument
    :return: arg_a - arg_b

    Example usage::

      >>> from python_library import function_c
      >>> function_c(2, 3)
      -1
    """
    return arg_a - arg_b
