Scripts
=======

Information about scripts

``convert_file.sh``
~~~~~~~~~~~~~~~~~~~

Converts files

**Usage:**

.. code::

  convert_file.sh in_file out_file

.. program:: convert_file.sh

**Options:**

.. option:: in_file

  Input file name

.. option:: out_file

  Output file name

``make_file.sh``
~~~~~~~~~~~~~~~~

Makes files

**Usage:**

.. code::

  make_file.sh out_file

.. program:: make_file.sh

**Options:**

.. option:: in_file

  Input file name

``remove_file.sh``
~~~~~~~~~~~~~~~~~~

Removes a file

**Usage:**

.. code::

  remove_file.sh file_name

.. program:: remove_file.sh

**Options:**

.. option:: file_name

  Name of file to remove
