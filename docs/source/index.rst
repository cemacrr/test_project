.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:
   
   Home <self>
   python_library
   scripts
   links

.. include:: ../../README.rst

Title A
=======

Some text

Title B
-------

Some more text

Title C
~~~~~~~

Another bit of text.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
